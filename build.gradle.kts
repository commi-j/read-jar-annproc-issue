//
group = "tk.labyrinth"
version = "0.1.0-SNAPSHOT"
//
plugins {
	`java-library`
}
//
subprojects {
	apply {
		`java-library`
	}
	//
	repositories {
		mavenCentral()
	}
	//
	tasks {
		//
		compileJava {
			sourceCompatibility = "11"
			targetCompatibility = "11"
		}
	}
}
//
// Utility
inline val ObjectConfigurationAction.`java-library`: ObjectConfigurationAction
	get() = plugin("java-library")
