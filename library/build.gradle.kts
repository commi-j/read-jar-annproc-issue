//
dependencies {
	val autoServiceVersion = "1.0-rc6"
	//
	compileOnly("com.google.auto.service", "auto-service", autoServiceVersion)
	annotationProcessor("com.google.auto.service", "auto-service", autoServiceVersion)
}
