package tk.labyrinth;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@AutoService(Processor.class)
@SupportedAnnotationTypes("*")
public class MyProcessor extends AbstractProcessor {

	private static final Object lock = new Object();

	private boolean happened = false;

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		synchronized (lock) {
			if (!happened) {
				happened = true;
				try {
					List<URL> urls = new ArrayList<>();
					MyProcessor.class.getClassLoader().getResources("file.txt").asIterator().forEachRemaining(urls::add);
					for (URL url : urls) {
						try (InputStream is = url.openStream()) {
							new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).lines()
									.forEach(line -> System.out.println("LINE: " + line));
						}
					}
					System.out.println(urls);
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
			}
			return false;
		}
	}
}
